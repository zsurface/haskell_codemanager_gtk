{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
import Control.Monad
import Data.Char
import qualified Data.HashMap.Strict as M 
import qualified Data.Set as DS
import qualified Data.List as DL
import Data.List.Split
import Data.List(groupBy)
--import qualified Data.Map as M 
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import AronModule 
import Graphics.UI.Gtk

-- | Search snippets file 
-- | Integrate it with GTK2H
-- | -------------------------------------------------------------------------------- 
-- | Compile: ghc --make codemanager.hs -o codemanager
-- | Or run: codemanager.sh
-- | -------------------------------------------------------------------------------- 
-- | dependence: $h/AronModule.hs

--path = "/Users/cat/myfile/bitbucket/snippets/snippet_test.m"
path = "/Users/cat/myfile/bitbucket/snippets/snippet.hs"

getInput::IO (M.HashMap String [[String]])
getInput = do
        pplist <- readSnippet path
        let keylist = DL.map(\x -> 
                                (foldr(++) [] $ DL.map(\y -> prefix y) (fst x),
                                 snd x
                                )
                            ) pplist 
        let mymap = map(\cx -> [(x, y) | x <- fst cx, y <- [snd cx]]) keylist
        let lmap = foldr(++) [] mymap
        let sortlist = qqsort(\x y -> f x y) lmap 
                                where f x y = fst x > fst y
        let mmap = M.fromList lmap
        let group= groupBy(\x y -> f x y) sortlist 
                              where f x y = fst x == fst y
        let uzip = map(\x -> unzip x) group
        let kmap = map(\x -> (head . fst $ x, snd x)) uzip
        let hmap = M.fromList kmap
        return hmap

saveText :: Entry -> TextBuffer -> (M.HashMap String [[String]])-> IO ()
saveText fld txtBuf hmap = do
    key <- entryGetText fld
    case (M.lookup key hmap) of
        Just s -> textBufferSetText txtBuf (foldr(++) [] $ (map unlines s))
        _      -> textBufferSetText txtBuf "nothing" 
    return ()

main :: IO ()
main= do
  initGUI
  window <- windowNew
  set window [windowTitle := "Text Entry", containerBorderWidth := 10]
  hmap <- getInput

  vb <- vBoxNew False 0
  containerAdd window vb

  hb <- hBoxNew False 0
  boxPackStart vb hb PackNatural 0

  txtfield <- entryNew
  boxPackStart hb txtfield PackNatural 5

  tvBuf <- textBufferNew Nothing
  textView <- textViewNewWithBuffer tvBuf
  boxPackStart vb textView PackNatural 2 

  widgetShowAll window

  onEntryActivate txtfield (saveText txtfield tvBuf hmap)
  onDestroy window mainQuit
  mainGUI
